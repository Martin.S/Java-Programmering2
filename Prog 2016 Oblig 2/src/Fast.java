
public interface Fast {

	public abstract void setForNavn(String navn);
	public abstract String getForNavn();
	public abstract void setEtterNavn(String navn);
	public abstract String getEtterNavn();
	public abstract void setFultNavn(String navn);
	public abstract String getFultNavn();
	public double beregnKreditt();
	public double beregnBonus();


}

