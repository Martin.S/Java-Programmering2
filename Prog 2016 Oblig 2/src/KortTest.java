import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class KortTest {
	private static Scanner scan;

	public static void main(String[] args) {
		scan = new Scanner(System.in);
		ArrayList<Kort> reg = new ArrayList<Kort>();

		Kort k1 = new Ansatt("Ole Olsen", 1234);
		Kort k2 = new Gjest("Marit Olsen", 9999);
		Kort k3 = new Ansatt ("Karl Norman", 5555);
		Kort k4 = new Ansatt("Arild Olsen", 1234);
		Kort k5 = new Ansatt("Berit Olsen", 1234);
		Kort k6 = new Ansatt("Ole Antonsen", 1234);
		k3.setSperretKort(true);
		
		reg.add(k1);
		reg.add(k2);
		reg.add(k3);
		reg.add(k4);
		reg.add(k5);
		reg.add(k6);
		
Collections.sort(reg);

		
		
		for (int i = 0; i < reg.size(); i++) {
			Kort kort = (Kort) reg.get(i);
			System.out.println("\n" + kort);
			System.out.println("Kort Nummer er: " + Kort.getKortNummer());
			System.out.println("\nTest av kort: med kode 1234 er" + (kort.checkPin(1234) ? " gyldig" : " ugyldig"));
			System.out.println("\nTest av kort: med kode 9999 er" + (kort.checkPin(9999) ? " gyldig" : " ugyldig"));
		}
		System.out.println("\n\n");
		

		//System.out.println(k1.getFultNavn());
		//D�r system
		//doorSystem(k1);

	}

	/*
	
	public static void doorSystem(Kort n) {
		scan = new Scanner(System.in);
		Kort user = n;
		System.out.println("User: " + user.getKortNavn());

		if (user.sperretKort == false) {
			if (user.officeTime() == true && user.isGjest() == false) {
				System.out.println("Access granted!");

			} else if (user.officeTime() == false && user.isGjest() == false) {
				System.out.println("School closed, please enter PIN: ");
				int PIN = scan.nextInt();

				if (user.checkPin(PIN) == true) {
					System.out.println("Access granted!");
				} else {
					System.out.println("Access denied!");
				}

			} else if (user.officeTime() == true && user.isGjest() == true) {
				System.out.println("Enter PIN");
				int PIN = scan.nextInt();
				if (user.checkPin(PIN) == true) {
					System.out.println("Access granted!");
				} else {
					System.out.println("Access denied!");
				}

			} else if (user.officeTime() == false && user.isGjest() == true) {
				System.out.println("Access denied!");

			} else {
				System.out.println("Error!");
			}
		} else {
			System.out.println("Card not valid!");

		}
	}	*/					
}