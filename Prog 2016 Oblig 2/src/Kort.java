import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public abstract class Kort implements Fast, Comparable<Kort>, Cloneable {
	static protected int kortNummer;
	protected boolean sperretKort;
	protected boolean gjest = false;
	protected int kortPin = 0;
	protected String kortNavn;
	protected ArrayList<Integer> duplicateList = new ArrayList();

	protected String forNavn;
	protected String etterNavn;

	public Kort() {
		kortNummer = getRandomNumber();
		kortPin = 9999;
		sperretKort = false;
		gjest = true;
		kortNavn = "Gjest";
		setFultNavn("Gjest");

	}

	public Kort(int kortPin, boolean gjest, String kortNavn) {
		kortNummer = getRandomNumber();
		this.kortPin = kortPin;
		this.gjest = gjest;
		setFultNavn(kortNavn);
		sperretKort = false;

	}

	protected int getRandomNumber() {

		Random rnd = new Random();
		int n = 100000 + rnd.nextInt(999999);

		if (!duplicateList.contains(n)) {
			duplicateList.add(n);
			return n;

		} else {
			return getRandomNumber();
		}

	}

	abstract boolean checkPin(int i);

	public boolean officeTime() {
		try {
			String timeEnd = "07:00:00";
			Date EndTime = new SimpleDateFormat("HH:mm:ss").parse(timeEnd);

			String timeStart = "17:00:00";
			Date startTime = new SimpleDateFormat("HH:mm:ss").parse(timeStart);

			String currentTime = new SimpleDateFormat("HH:mm:ss").format(new Date());
			Date i = new SimpleDateFormat("HH:mm:ss").parse(currentTime);

			long x = i.getTime();
			if (i.before(startTime) && i.after(EndTime) || i.equals(startTime) || i.equals(EndTime)) {
				return true;
			} else {
				return false;
			}
		} catch (ParseException e) {
			System.out.println("Error");
			return false;
		}

	}

	public static int getKortNummer() {
		return kortNummer;
	}

	public static void setKortNummer(int kortNummer) {
		Kort.kortNummer = kortNummer;
	}

	public boolean isSperretKort() {
		return sperretKort;
	}

	public void setSperretKort(boolean sperretKort) {
		this.sperretKort = sperretKort;
	}

	public boolean isGjest() {
		return gjest;
	}

	public void setGjest(boolean gjest) {
		this.gjest = gjest;
	}

	public int getKortPin() {
		return kortPin;
	}

	public void setKortPin(int kortPin) {
		this.kortPin = kortPin;
	}

	public String getKortNavn() {
		return kortNavn;
	}

	public void setKortNavn(String kortNavn) {
		this.kortNavn = kortNavn;
	}

	@Override
	public String toString() {
		return "Kort [sperretKort=" + sperretKort + ", gjest=" + gjest + ", kortPin=" + kortPin + ", kortNavn="
				+ kortNavn + ", duplicateList=" + duplicateList + "]";
	}

	@Override
	public void setForNavn(String forNavn) {
		this.forNavn = forNavn;
	}

	@Override
	public String getForNavn() {
		return forNavn;
	}

	@Override
	public void setEtterNavn(String etterNavn) {
		this.etterNavn = etterNavn;

	}

	@Override
	public String getEtterNavn() {
		return etterNavn;
	}

	@Override
	public void setFultNavn(String fultNavn) {
		String[] split = fultNavn.split("\\s+");
		try {
		forNavn = split[0];
		etterNavn = split[1];
		} catch (ArrayIndexOutOfBoundsException e) {
	         forNavn = split[0];
	         etterNavn = "";
		}
	}

	@Override
	public String getFultNavn() {
		String string1 = forNavn + " " + etterNavn;
		return string1;
	}

	@Override
	public double beregnKreditt() {
		// Ikke beskrevet i oppgaven
		return 0;
	}

	@Override
	public double beregnBonus() {
		// Ikke beskrevet i oppgaven
		return 0;
	}

	@Override
	public int compareTo(Kort k) {
		if (!(this.getEtterNavn().compareTo(k.getEtterNavn()) == 0)){
			return this.getEtterNavn().compareTo(k.getEtterNavn());
		}else{
			return this.getForNavn().compareTo(k.getForNavn());
			}
		}
			
		}

