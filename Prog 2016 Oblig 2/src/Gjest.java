
public class Gjest extends Kort {

	public Gjest(String kortNavn, int kortPin) {
		super.kortPin = kortPin;
		super.gjest = true;
		// super.kortNavn = kortNavn;
		super.setFultNavn(kortNavn);
		super.sperretKort = false;
		super.kortNummer = super.getRandomNumber();

	}

	@Override
	boolean checkPin(int i) {
		if (sperretKort == true){
			return false;
		}
		else if (i == kortPin) {
			return true;
		} else {
			return false;
		}
	}

}
