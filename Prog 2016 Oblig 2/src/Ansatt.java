
public class Ansatt extends Kort {

	public Ansatt() {

	}

	public Ansatt(String kortNavn, int kortPin) {
		setFultNavn(kortNavn);
		super.gjest = false;
		super.kortNavn = kortNavn;
		super.sperretKort = false;
	}

	@Override
	boolean checkPin(int i) {
		if (sperretKort == true){
			return false;
		}
		else if (i == kortPin) {
			return true;
		} else {
			return false;
		}
	}
}